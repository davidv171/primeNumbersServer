import sympy
import sqlite3
from shutil import copyfile
import sys
import random,string
import base64
from cryptography.fernet import Fernet

db_filename = "users.db"
backup_filename = "oldusers.db"
#Copy db_filename file into backup_filename file before the database is overwritten!
try:
    copyfile(db_filename,backup_filename)
except IOError:
    print("File error, check if you're root and file exists!")
    contDummy = input("Continue?(y/n) ")
    if contDummy is "n" or contDummy is "no":
        sys.exit(0)


users_conn = sqlite3.connect('users.db')
users_cursor = users_conn.cursor()
users_cursor.execute('''DROP TABLE IF EXISTS entries''')
users_cursor.execute('''CREATE TABLE IF NOT EXISTS entries(id INTEGER,key STRING)''')
#CREATE A SAMPLE ADMIN ACCOUNT,IT HAS AN ID OF 0 (EVERY ADMIN ACCOUNT HAS AN ID OF 0)
#AND AN API KEY
#RANDOMLY GENERATE 24-character KEY AND PUT IT INTO A DATABASE,

char_num = 24
allchar = string.ascii_letters + string.digits
random_api = "".join(random.choice(allchar) for x in range(char_num))
print("generated api ")
print(random_api)
crypt_key = Fernet.generate_key()
fernet_key = Fernet(crypt_key)
print("------------------------------------------------------------------------------------")
print(crypt_key)
print("------------------------------------------------------------------------------------")
encrypted_api = fernet_key.encrypt(bytes(random_api,encoding="ascii"))
users_cursor.execute('insert into entries values(?,?)',(0,encrypted_api,))
#c.execute('insert into entries values(?,?)',(1,encrypted_api,))

print("Encrypted API")
users_conn.commit()
users_conn.close()


sys.exit(0)
