# primeNumbersServer
A simple server, that returns if a number is prime or not.
Runs on SQLite3. Contains a class that generates a bunch of prime numbers, adds it to a database. The user then requests a number and the server returns if the requested number is prime or not.

## Database ##

Uses an SQLite3 database. One stores users and one stores the prime numbers. Users have two possible levels.
### Users.db: ###
- Admins:
  Statically written, has an ID of 0 in the users database. Their privilege is to be able to submit a new prime number without it being checked. It is added to the database as soon as it is submitted.
- Users:
  Can be registered at any time, any prime number submission they send has to be reviewed before being submitted to the database. Have an ID of one in the users database.
Both layers have the same 16 character randomly generated string as their api key. In the database the attribute is written as string type "key". And the id is written as and integer "id".
### Primeornot.db ###
Uses the primenumbergenerator.py to generate a certain amount of prime numbers, that are then written into the database. There is only one value in each entry and that is "number" of type integer. There are only prime numbers in the database to simplify the lookups.

## Server ##

The server runs on your localhost with the specified port of 500. It listens to HTTP requests and if it recognizes a request it either returns a json that tells you if the number you've requested it prime or not. It lets you submit new numbers and admin or an regular user but the overall functionality does not require any type of logging in or registration.

### Requests ###

#### / ####

Is the "homepage" of the server. It should return directions on how to use the API. 

Currently returns: 'This is the homepage. Call the number and it returns if its prime or not'
