import os
import sympy
import sqlite3
from shutil import copyfile
#needs to install sympy
import sys
from cryptography.fernet import Fernet
prime_number_db = "primerornot.db"
backup_filename = "oldprimes.db"

filename_biggest_prime = "bigolprime.txt"
prime_conn = sqlite3.connect(prime_number_db)
cursor_prime = prime_conn.cursor()
#Avoid SQL injections
#Delete if table does exist, create a new table if it doesnt exist
#Makes sure we dont have many entries of the same number!
cursor_prime.execute('''DROP TABLE IF EXISTS entries''')
#TODO: The encryption process will then be replicated with dummyUserGenerator.py
#TODO: Backup old database

#Copy db_filename file into backup_filename file before the database is overwritten!
try:
    copyfile(prime_number_db,backup_filename)
except IOError:
    print("File error, check if you're root and file exists!")
    contDummy = input("Continue?(y/n) ")
    if contDummy is "n" or contDummy is "no":
        sys.exit(0)


cursor_prime.execute('''create table if not exists entries(number INTEGER)''')
print("Input the prime number range")

start_from = int(input("Starting from:"))
end_at = int(input("End at:"))
list_prime_numbers = list(sympy.primerange(start_from,end_at))
size_prime_numbers = len(list_prime_numbers)
highest_generated_prime = list_prime_numbers[size_prime_numbers-1]
for x in range(0, len(list_prime_numbers)):
    cursor_prime.execute('insert into entries values(?)', (list_prime_numbers[x],))
#database contains , after each entry, it's a syntax convention, ignoring it returns a typeerror
all_numbers = cursor_prime.execute('select * from entries')
print(all_numbers.fetchall())
prime_conn.commit()
prime_conn.close()



#Encrypt highest number into a text file, whose name is given by filename_biggest_prime
crypt_key = Fernet.generate_key()
fernet_key = Fernet(crypt_key)
string_highest_prime = str(highest_generated_prime)
print("Crypt key(save this for admin work):")
print("------------------------------------------------------------------------------------")
print(crypt_key)
print("------------------------------------------------------------------------------------")
encrypted_number = fernet_key.encrypt(bytes(string_highest_prime,encoding="ascii"))
print("The encrypted message is:")
print(encrypted_number)
#Save encrypted number into a file
file = open(filename_biggest_prime,mode="w")
file.write(str(encrypted_number))
decrypted_number = int(fernet_key.decrypt(encrypted_number))
print(decrypted_number)
#decrypted_number = int(fernet_key.decrypt(encrypted_number))
sys.exit(0)
